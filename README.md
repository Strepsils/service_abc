server_a: aiohttp service with REST API, allowed 2 methods(GET, PUT), url: /v1/dictionary/  
worker_c: asyncio worker, get or put records to redis db  
rabbitmq as part B: have one queue for requests from server_a, and for each response one queue from worker_c  
For get request use url like: "/v1/dictionary/{key}", where key is key of value on redis.  
For put request use PUT http method to url: "/v1/dictionary", body of request must be json dict with parameters "key" and "value"  
example: '{"key": "key_used_in_redis", "value": "value_on_this_key"}', after you just can check it by get request "/v1/dictionary/key_used_in_redis"  
Start services: docker-compose up server_a worker_c  

FROM python:3.6-onbuild

ENV PYTHONUNBUFFERED 1
ENV APP /app
ENV PYTHONPATH .:./service_abc

RUN mkdir $APP

WORKDIR $APP

COPY requirements.txt .
COPY . $APP

COPY entrypoint.sh /usr/bin/entrypoint
RUN chmod 755 /usr/bin/entrypoint

ENTRYPOINT ["entrypoint"]

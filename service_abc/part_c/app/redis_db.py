import aioredis


class RedisController:

    def __init__(self, url):
        self.url = url
        self.conn = None

    async def connect(self):
            self.conn = await aioredis.create_connection(self.url)

    async def get(self, key):
        return await self.conn.execute('GET', key)

    async def put(self, key, value):
        return await self.conn.execute('SET', key, value)

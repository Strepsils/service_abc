import aioamqp


class AmqpClient:

    def __init__(self, url=None):
        self._protocol = None
        self._transport = None
        self._url = url

    async def connect(self):
        self._transport, self._protocol = await aioamqp.from_url(self._url)

    async def get_channel(self):
        if self._protocol:
            return await self._protocol.channel()
        raise Exception("Protocol not defined")

    async def close_connection(self):
        await self._protocol.close()
        self._transport.close()

import aioamqp
import json
import logging

from part_c.app.amqp import AmqpClient
from part_c.app.redis_db import RedisController
from part_c.app.producer import Producer

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class Worker(AmqpClient):

    REQUESTS_QUEUE = 'requests'

    def __init__(self, loop, amqp_url, redis_url):
        self.loop = loop
        self.redis = RedisController(redis_url)
        self.producer = Producer(amqp_url)
        super().__init__(url=amqp_url)

    async def run(self):
        await self.connect()
        self.channel = await self.get_channel()
        await self.producer.setup()
        await self.redis.connect()
        self.loop.create_task(self.consume())

    async def process_msg(self, channel, body, envelope, properties):
        log.debug(f'in process_msg body: {body}')
        try:
            msg = json.loads(body)
        except json.JSONDecodeError as e:
            log.debug(e)
        else:
            action = properties.type
            if action == 'get':
                value = await self.redis.get(msg['key'])
                if value is None:
                    value = "None"
                if isinstance(value, bytes):
                    value = value.decode()
                await self.producer.produce(
                    value,
                    properties.reply_to,
                )
            if action == 'put':
                result = await self.redis.put(msg['key'], msg['value'])
                await self.producer.produce(
                    result.decode(),
                    properties.reply_to
                )

    async def consume(self):
        await self.channel.queue_declare(queue_name=self.REQUESTS_QUEUE)
        log.debug(' [*] Waiting for requests.')
        await self.channel.basic_consume(
            queue_name=self.REQUESTS_QUEUE,
            callback=self.process_msg,
            no_ack=True
        )

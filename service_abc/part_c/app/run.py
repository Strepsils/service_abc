import asyncio
import logging

from part_c.app.worker import Worker
from part_c.app.config import AMQP_URL, REDIS_URL

logging.basicConfig(level=logging.DEBUG)

loop = asyncio.get_event_loop()
worker = Worker(loop, AMQP_URL, REDIS_URL)
loop.run_until_complete(worker.run())
loop.run_forever()
loop.close()

import json
import logging

from part_c.app.amqp import AmqpClient


log = logging.getLogger(__name__)


class Producer(AmqpClient):

    def __init__(self, amqp_url):
        self.channel = None
        super().__init__(url=amqp_url)

    async def setup(self):
        await self.connect()
        self.channel = await self.get_channel()

    async def produce(self, payload, queue_for_response):
        body = json.dumps(payload)
        log.debug(
            f"PRODUCE EXECUTED: body={body},"
            f"queue for resp={queue_for_response}"
        )
        log.debug("Declarated queue")
        await self.channel.basic_publish(
            payload=body,
            exchange_name='',
            routing_key=queue_for_response,
        )
        log.debug('published...')

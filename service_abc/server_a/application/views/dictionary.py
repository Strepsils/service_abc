from aiohttp import web
from json import JSONDecodeError

from server_a.application.utils import key_validator


async def get_record(request):
    key = request.match_info['key_name']
    controller = request.app.dictionary_controller
    result = await controller.get(key)
    return web.json_response(dict(key=key, value=result))


async def put_record(request):
    try:
        data = await request.json()
    except JSONDecodeError:
        return web.json_response({"result": "Not valid request"})
    result, error_msg = key_validator(data['key'])
    if not result:
        return web.json_response(error_msg)
    controller = request.app.dictionary_controller
    result = await controller.put(data['key'], data['value'])
    return web.json_response(dict(result=result))

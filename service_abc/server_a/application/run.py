import logging
from aiohttp import web

from server_a.application import settings, amqp, controller, views


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

app = web.Application(logger=log, debug=settings.DEBUG)
app.settings = settings
app.on_startup.append(amqp.on_app_start)
app.on_startup.append(controller.on_app_start)

app.add_routes(
    [
        web.get(r'/v1/dictionary/{key_name}', views.get_record),
        web.put(r'/v1/dictionary/', views.put_record),
    ]
)

if __name__ == '__main__':
    web.run_app(app, host='0.0.0.0', port=9999)

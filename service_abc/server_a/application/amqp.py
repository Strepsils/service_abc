import aioamqp
import json
import logging


log = logging.getLogger(__name__)


class AmqpClient:

    def __init__(self, url):
        self.transport = None
        self.protocol = None
        self._url = url

    async def create_connection(self):
        self.transport, self.protocol = await aioamqp.from_url(self._url)

    async def get_channel(self):
        return await self.protocol.channel()

    async def close_connections(self):
        await self.protocol.close()
        self.transport.close()


async def on_app_start(app):
    amqp_client = AmqpClient(app.settings.AMQP_URL)
    await amqp_client.create_connection()
    app.amqp_client = amqp_client

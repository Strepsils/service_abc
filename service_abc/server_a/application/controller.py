import json
import logging
import asyncio
from enum import Enum

log = logging.getLogger(__name__)


class DictionaryController:

    REQUESTS_QUEUE = 'requests'

    def __init__(self, amqp_client):
        self.amqp_client = amqp_client

    class Action(Enum):
        GET = 'get'
        PUT = 'put'

    class ConsumeError(Exception):
        pass

    async def generate_request(self, action, key, value=None):
        result = asyncio.Future()
        payload = {"key": key}
        if value is not None:
            payload.update(value=value)
        channel_produce = await self.amqp_client.get_channel()
        channel_consume = await self.amqp_client.get_channel()
        declared = await channel_produce.queue_declare(exclusive=True)
        reply_to = declared['queue']
        await self.produce(payload, action.value, channel_produce, reply_to)
        await self.consume(reply_to, to_future=result, chan=channel_consume)
        response = await result
        await channel_produce.queue_delete(reply_to)
        await channel_produce.close()
        await channel_consume.close()
        return response

    async def get(self, key):
        return await self.generate_request(self.Action.GET, key)

    async def put(self, key, value):
        return await self.generate_request(self.Action.PUT, key, value)

    async def produce(self, payload, action, channel, reply_to):
        try:
            body = json.dumps(payload)
        except json.JSONDecodeError:
            log.debug("Json decode error")
            return

        await channel.queue_declare(queue_name=self.REQUESTS_QUEUE)

        await channel.basic_publish(
            payload=body,
            exchange_name='',
            routing_key=self.REQUESTS_QUEUE,
            properties={
                'reply_to': reply_to,
                'type': action,
            }
        )

    async def consume(self, queue_name, to_future, chan):
        async def get_msg(channel, body, envelope, properties):
            msg = None
            try:
                msg = json.loads(body)
            except json.JSONDecodeError:
                log.debug("Json decode error")
                to_future.set_exception(self.ConsumeError)
            to_future.set_result(msg)

        await chan.basic_consume(
            queue_name=queue_name,
            callback=get_msg,
            no_ack=True
        )


async def on_app_start(app):
    app.dictionary_controller = DictionaryController(app.amqp_client)

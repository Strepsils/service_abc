def key_validator(key: str):
    msg = None
    if '/' in key:
        msg = {"result": "Not valid key! Key can't contains '/' token"}
        return False, msg
    return True, msg
